# README #

### What is this repository for? ###

* Name - Sample-Report TEST
* Version 1.0
* IDE NetBeans 8.2

### Framework Esterni ###

-SuperCSV (creazione del csv)
-Apache POI (creazione xlsx)
-log4j (logger)
-ojdbc6 (connessione al db oracle)

### File Esterni ###

- REPORT_EXCEL.xlsx
- REPORT_CSV.csv
- config.properties (parametri per la connessione al db)
- log4j2.xml (parametri per il log4j)

### Project ###

0- Prima fase di inizializzazione variabili, connessione al DB.
1- Lettura di tutte le Countries dal WebService ed inserirle nell'oggetto "Countries".
2- Prendere le altre informazioni richieste dal WS utilizzando quest'ultimo ciclando per Country ed inserirle sul DB.
3- Leggere dal DB tutti i record ed in base alle condizioni stabilite (GMT/Currency = null -> Log ERROR / GMT <> "+h" -> Log WARNING e SKIP)
4- Nella lettura viene creato un oggetto di tipo Report (per la creazione del CSV) e viene popolato man mano il file xlsx
5- Quando il progetto viene compilato crea un jar standard senza dipendenze e un jar con le dipendenze.
