/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package como.francesco.samplereport;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author frcomo
 */
public class ConnectionDB {

    public Connection connect() throws InterruptedException, ClassNotFoundException, FileNotFoundException, IOException {

        try {
            Properties prop = new Properties();
            InputStream input = new FileInputStream("config.properties");

            // load a paroperties file
            prop.load(input);

            Connection conn;
            //String usr = "test";
            //String pwd = "test";
            //String url = "jdbc:oracle:thin:@(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 1521)) (CONNECT_DATA = (SID = XE)))";
            String usr = prop.getProperty("user");
            String pwd = prop.getProperty("password");
            String url = prop.getProperty("url");

            Class.forName("oracle.jdbc.driver.OracleDriver");
            conn = DriverManager.getConnection(url, usr, pwd);
            conn.setAutoCommit(false);
            //logDB.info("## Connessione istaurata con successo ##");
            return conn;
        } catch (SQLException e) {
            e.getMessage();
            System.exit(10);
        }
        return null;
    }
}
