/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package como.francesco.samplereport;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.stream.XMLStreamException;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Francesco Como
 */
public class MainClass {

    public static Countries a;
    public static Logger log;

    public static void main(String[] args) {

        log = Logger.getLogger(MainClass.class);
        
        try {
            //Variables
            ConnectionDB db = new ConnectionDB();
            Connection conn = db.connect();

            //Excel
            SXSSFWorkbook wb = new SXSSFWorkbook(); // -> for workbook with large dimensions
            wb.setCompressTempFiles(true);

            ICsvBeanWriter beanWriter;

            beanWriter = new CsvBeanWriter(new FileWriter("REPORT_CSV.csv"), CsvPreference.STANDARD_PREFERENCE);
            //COUNTRY_NAME, COUNTRY_CODE, GMT, CURRENCY_CODE,CURRENCY
            final String[] header = new String[]{"COUNTRY_NAME", "COUNTRY_CODE", "GMT", "CURRENCY_CODE", "CURRENCY", "LOCALTIME", "LOCALTIMEGMT"};
            final CellProcessor[] processors = getProcessors();

            // write the header
            beanWriter.writeHeader(header);

            //Excel Style
            //Set Font Label
            XSSFFont fontLabel = (XSSFFont) wb.createFont();
            fontLabel.setFontHeightInPoints((short) 9);
            fontLabel.setFontName("Arial");
            fontLabel.setColor(IndexedColors.BLACK.getIndex());
            fontLabel.setBold(true);
            fontLabel.setItalic(false);

            //Set Font Body
            XSSFFont font = (XSSFFont) wb.createFont();
            font.setFontHeightInPoints((short) 8);
            font.setFontName("Arial");
            font.setColor(IndexedColors.BLACK.getIndex());
            font.setBold(false);
            font.setItalic(false);

            //Set Font Date
            CellStyle styleDate = wb.createCellStyle();
            CreationHelper createHelper = wb.getCreationHelper();
            styleDate.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));

            //Set CellStyle Label
            CellStyle styleLabel = wb.createCellStyle();
            styleLabel.setFont(fontLabel);

            //Set CellStyle Body
            CellStyle styleBody = wb.createCellStyle();
            styleBody.setFont(font);

            //All Countries
            String uri = "http://www.webservicex.net/country.asmx/GetCountries";
            getWebService(uri);

            Countries c = (Countries) a.clone();

            String insertTableSQL = "INSERT INTO REPORT"
                    + "(COUNTRY_NAME, COUNTRY_CODE, GMT, CURRENCY_CODE,CURRENCY) VALUES"
                    + "(?,?,?,?,?)";

            String selectTableSQL = "select COUNTRY_NAME, COUNTRY_CODE, GMT, CURRENCY_CODE,CURRENCY from report";

            PreparedStatement preparedStatement = conn.prepareStatement(insertTableSQL);

            for (Country b : c.getCoutries()) {


                //GetGMT
                uri = "http://www.webservicex.net/country.asmx/GetGMTbyCountry?CountryName=" + URLEncoder.encode(b.getName(), "UTF-8");
                getWebService(uri);

                b.setGMT(a.getCoutries().get(0).getGMT());

                //GetCurrency & Currency Code
                uri = "http://www.webservicex.net/country.asmx/GetCurrencyByCountry?CountryName=" + URLEncoder.encode(b.getName(), "UTF-8");
                getWebService(uri);

                b.setCountryCode(a.getCoutries().get(0).getCountryCode());
                b.setCurrency(a.getCoutries().get(0).getCurrency());
                b.setCurrencyCode(a.getCoutries().get(0).getCurrencyCode());

                preparedStatement.setString(1, b.getName());
                preparedStatement.setString(2, b.getCountryCode());
                preparedStatement.setString(3, b.getGMT());
                preparedStatement.setString(4, b.getCurrencyCode());
                preparedStatement.setString(5, b.getCurrency());
                preparedStatement.addBatch();

                //System.exit(0);
            }

            preparedStatement.executeBatch();
            conn.commit();
            preparedStatement.close();

            /*
        Get data from DB
             */
            preparedStatement = conn.prepareStatement(selectTableSQL);

            ResultSet rs = preparedStatement.executeQuery();

            ResultSetMetaData rsmd = rs.getMetaData();

            int columns = rsmd.getColumnCount(); //+2= LocalTime e LocalTime + gmt

            String sheetName = "Report";
            SXSSFSheet sheet = (SXSSFSheet) wb.createSheet(sheetName);
            sheet.setRandomAccessWindowSize(100); // -> the records maximum the file out flush

            int idrow = 0;
            Row row = sheet.createRow(idrow);

            {
                int i;
                for (i = 0; i < columns; i++) {
                    row.createCell(i).setCellValue(rsmd.getColumnLabel(i + 1));
                    row.getCell(i).setCellStyle(styleLabel);
                }

                row.createCell(i).setCellValue("LOCAL TIME (Rome)");
                row.getCell(i).setCellStyle(styleLabel);
                row.createCell(i + 1).setCellValue("LOCAL TIME (Rome) + GMT");
                row.getCell(i + 1).setCellStyle(styleLabel);

            }

            row = sheet.createRow(++idrow);
            ZoneId zoneIdRome = ZoneId.of("Europe/Rome");
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
            BeanProcessor bp = new BeanProcessor();

            List<Report> lRep = new ArrayList();

            while (rs.next()) {
                if (rs.getString("GMT") == null || rs.getString("CURRENCY") == null) {
                    log.error(rs.getString("COUNTRY_NAME") + ": Countries without GMT or currency");
                    continue; //SKIP se GMT o CURRENCY = "" (vuoto)
                } else if (!rs.getString("GMT").matches(("^(\\+|\\-)([1-9])"))) {
                    log.warn(rs.getString("COUNTRY_NAME") + ": GMT diverso da +h");
                    continue; //SKIP se GMT è diverso da "+h"   
                }
                lRep.add((Report) bp.toBean(rs, Report.class));

                Report repObj = lRep.get(lRep.size() - 1);

                LocalTime ltRome = LocalTime.now(zoneIdRome);
                String lt = dtf.format(ltRome);
                //Errore##############
                String ltGMT = dtf.format(ltRome.atOffset(ZoneOffset.of(rs.getString("GMT"))));

                repObj.setLOCALTIME(lt);
                repObj.setLOCALTIMEGMT(ltGMT);

                //To Excel
                row.createCell(columns).setCellValue(ltGMT); //LocalTime + GMT
                row.getCell(columns).setCellStyle(styleDate);
                row.createCell(columns + 1).setCellValue(lt); //LocalTime
                row.getCell(columns + 1).setCellStyle(styleDate);

                for (int i = 0; i < columns; i++) {
                    row.createCell(i).setCellValue(rs.getString(i + 1));
                    row.getCell(i).setCellStyle(styleBody);
                }
                row = sheet.createRow(++idrow);
            }

            preparedStatement.close();
            conn.close();

            //To CSV
            for (Report r : lRep) {
                beanWriter.write(r, header, processors);
            }
            beanWriter.close();

            FileOutputStream fileOut = new FileOutputStream("REPORT_EXCEL.xlsx");

            wb.write(fileOut);
            fileOut.flush();
            fileOut.close();
            wb.dispose();

        } catch (InterruptedException | ClassNotFoundException | IOException | SQLException | CloneNotSupportedException | JAXBException | SAXException | ParserConfigurationException ex) {
            log.error(ex);
        }
    }

    static void getWebService(String uri) throws MalformedURLException, IOException, ParserConfigurationException, SAXException, JAXBException, InterruptedException {

        URL url = new URL(uri);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestMethod("GET");
        connection.connect();

        if (connection.getResponseCode() != 200) {
            log.error("Error HTTP: " + connection.getResponseCode());
            return;
            //is = connection.getErrorStream();
        }

        connection.getInputStream();
        DocumentBuilderFactory objDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder objDocumentBuilder = objDocumentBuilderFactory.newDocumentBuilder();

        Document doc = objDocumentBuilder.parse(connection.getInputStream());

        NodeList ResultNode = doc.getElementsByTagName("string");

        JAXBContext jc = JAXBContext.newInstance(Countries.class);

        a = (Countries) jc.createUnmarshaller().unmarshal(new StringReader(ResultNode.item(0).getTextContent()));

        connection.disconnect();
        Thread.sleep(100); //I set sleep because server after 3 times without sleep returns 403 error.
    }

    private static CellProcessor[] getProcessors() {

        final CellProcessor[] processors = new CellProcessor[]{
            new NotNull(),
            new Optional(),
            new Optional(),
            new Optional(),
            new Optional(),
            new Optional(),
            new Optional()
        };
        return processors;
    }

}
