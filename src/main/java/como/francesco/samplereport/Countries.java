/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package como.francesco.samplereport;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author frcomo
 */
@XmlRootElement(name = "NewDataSet")
@XmlAccessorType(XmlAccessType.FIELD)
public class Countries implements Cloneable {

    @XmlElement(name = "Table")
    public ArrayList<Country> NewDataSet;

    public List<Country> getCoutries() {
        return NewDataSet;
    }
    
    @Override
	 public Object clone() throws CloneNotSupportedException {
	 return super.clone();
	 }
}
