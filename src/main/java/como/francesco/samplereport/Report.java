/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package como.francesco.samplereport;

/**
 *
 * @author frcomo
 */
public class Report {

    // COUNTRY_NAME, COUNTRY_CODE, GMT, CURRENCY_CODE,CURRENCY
    public String COUNTRY_NAME;
    public String COUNTRY_CODE;
    public String GMT;
    public String CURRENCY_CODE;
    public String CURRENCY;
    public String LOCALTIME;
    public String LOCALTIMEGMT;

    
        public String getLOCALTIME() {
        return LOCALTIME;
    }

    public void setLOCALTIME(String LOCALTIME) {
        this.LOCALTIME = LOCALTIME;
    }
    
        public String getLOCALTIMEGMT() {
        return LOCALTIMEGMT;
    }

    public void setLOCALTIMEGMT(String LOCALTIMEGMT) {
        this.LOCALTIMEGMT = LOCALTIMEGMT;
    }
    
    public String getCOUNTRY_NAME() {
        return COUNTRY_NAME;
    }

    public void setCOUNTRY_NAME(String COUNTRY_NAME) {
        this.COUNTRY_NAME = COUNTRY_NAME;
    }

    public String getCOUNTRY_CODE() {
        return COUNTRY_CODE;
    }

    public void setCOUNTRY_CODE(String COUNTRY_CODE) {
        this.COUNTRY_CODE = COUNTRY_CODE;
    }

    public String getGMT() {
        return GMT;
    }

    public void setGMT(String GMT) {
        this.GMT = GMT;
    }

    public String getCURRENCY_CODE() {
        return CURRENCY_CODE;
    }

    public void setCURRENCY_CODE(String CURRENCY_CODE) {
        this.CURRENCY_CODE = CURRENCY_CODE;
    }

    public String getCURRENCY() {
        return CURRENCY;
    }

    public void setCURRENCY(String CURRENCY) {
        this.CURRENCY = CURRENCY;
    }

    //public String localtime;
    //public String localtimeGMT;

    /*public Report(String name, String countryCode, String gmt, String currency, String currencyCode, String localtime, String localtimeGMT) {
        this.name = name;
        this.countryCode = countryCode;
        this.gmt = gmt;
        this.currency = currency;
        this.currencyCode = currencyCode;
        this.localtime = localtime;
        this.localtimeGMT = localtimeGMT;
    }*/
}
