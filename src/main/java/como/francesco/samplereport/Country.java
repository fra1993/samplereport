/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package como.francesco.samplereport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Francesco Como
 */
@XmlRootElement(name = "Table")
@XmlAccessorType(XmlAccessType.FIELD)
public class Country {
    
    @XmlElement(name = "Name")
    public String name;
    @XmlElement(name = "CountryCode")
    public String countryCode;
    @XmlElement(name = "GMT")
    public String gmt;
    @XmlElement(name = "Currency")
    public String currency;
    @XmlElement(name = "CurrencyCode")
    public String currencyCode;

    //Setter
    
    public void setName(String name) {
        this.name = name;
    }

    
    public void setCountryCode(String cc) {
        countryCode = cc;
    }

    
    public void setGMT(String gmt) {
        this.gmt = gmt;
    }

    
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    //Getter
    public String getName() {
        return name;
    }

    public String getCountryCode() {
        return this.countryCode;
   }

    public String getGMT() {
        return gmt;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

}
